class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    this._name = newName;
  }

  get age() {
    return this._age;
  }

  set age(newAge) {
    this._age = newAge;
  }

  get salary() {
    return this._salary;
  }

  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, langs) {
    super(name, age, salary);
    this._langs = langs;
  }

  get langs() {
    return this._langs;
  }

  set langs(newLangs) {
    this._langs = newLangs;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer('Svitlana Sky', 35, 10000, ['Ukrainian', 'English']);
const programmer2 = new Programmer('Alex Sea', 45, 12000, ['Polski', 'Deutsch']);

console.log(programmer1);
console.log(programmer2);
